// sgtools_libs - Code utilities for sgtools
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![cfg(feature = "config")]

use std::path::PathBuf;

use anyhow::anyhow;
use anyhow::Result;
use dirs::home_dir;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use serde::Serialize;

use super::prompt;

// GENERIC
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Config<C>
where
    C: ContextTrait,
{
    pub contexts: Vec<C>,
}

// GENERIC
pub trait ContextTrait
{
    fn get_name(&self) -> String;
    fn init<S: AsRef<str>>(&mut self, name: S) -> Result<()>;
}

// GENERIC
fn get_config_path<S: AsRef<str>>(app_name: S) -> Result<PathBuf>
{
    let home = match home_dir() {
        None => Err(anyhow!("Missing home dir"))?,
        Some(value) => value,
    };

    let mut config_path = PathBuf::new();
    config_path.push(home);
    config_path.push(format!(".{}.toml", app_name.as_ref()));
    // config_path.push(format!(".{}.toml", crate_name!()));

    Ok(config_path)
}

// GENERIC
fn read_config_file<CT, S: AsRef<str>>(app_name: S) -> Result<Config<CT>>
where
    CT: ContextTrait + Default + Serialize + DeserializeOwned,
{
    let config_path = get_config_path(&app_name)?;
    if !config_path.exists() {
        return init_config_file(app_name);
    }

    let content = std::fs::read_to_string(config_path)?;
    let data = match toml::from_str::<Config<CT>>(&content) {
        Ok(value) => value,
        Err(_) => init_config_file(app_name)?,
    };

    Ok(data)
}

// GENERIC
fn write_config_file<CT, S: AsRef<str>>(app_name: S, config: &Config<CT>) -> Result<()>
where
    CT: ContextTrait + Serialize,
{
    let config_path = get_config_path(app_name)?;
    let converted = toml::to_string_pretty(config)?;
    std::fs::write(config_path, converted)?;
    Ok(())
}

// GENERIC
fn init_config_file<CT, S: AsRef<str>>(app_name: S) -> Result<Config<CT>>
where
    CT: ContextTrait + Default + Serialize,
{
    let config = Config::default();
    write_config_file(app_name, &config)?;
    Ok(config)
}

// GENERIC
fn get_context<CT, S1: AsRef<str>, S2: AsRef<str>>(app_name: S1, name: &S2) -> Result<Option<CT>>
where
    CT: ContextTrait + DeserializeOwned + Serialize + Default + Clone,
{
    let config = match read_config_file::<CT, _>(app_name.as_ref()) {
        Ok(value) => value,
        Err(_) => init_config_file::<CT, S1>(app_name)?,
    };
    let context = config
        .contexts
        .iter()
        .find(|c| {
            c.get_name()
                .eq(name.as_ref())
        })
        .cloned();
    Ok(context)
}

// GENERIC
fn init_context<CT, S1: AsRef<str>, S2: AsRef<str>>(app_name: S1, name: &S2) -> Result<CT>
where
    CT: ContextTrait + DeserializeOwned + Serialize + Default + Clone,
{
    println!(
        "\x1b[91mx Context \"{}\" is not configured\x1b[0m",
        name.as_ref()
    );

    if !prompt::confirm("Configure the context?", true)? {
        return Err(anyhow!("Cancelled"));
    }

    let mut context = CT::default();
    context.init(name)?;

    let mut config = read_config_file::<CT, _>(app_name.as_ref())?;
    config
        .contexts
        .push(context.clone());
    write_config_file(app_name, &config)?;

    Ok(context)
}

// GENERIC
pub fn get_or_init<CT, S1: AsRef<str>, S2: AsRef<str>>(app_name: S1, context: S2) -> Result<CT>
where
    CT: ContextTrait + DeserializeOwned + Serialize + Default + Clone,
{
    match get_context::<CT, _, _>(app_name.as_ref(), &context)? {
        Some(value) => Ok(value),
        None => init_context::<CT, _, _>(app_name, &context),
    }
}

pub fn save_context<CT, S: AsRef<str>>(app_name: S, context: &CT) -> Result<()>
where
    CT: ContextTrait + DeserializeOwned + Serialize + Default + Clone,
{
    let mut config = read_config_file::<CT, _>(app_name.as_ref())?;
    // let mut contexts = config.contexts
    //     .iter()
    //     .filter(|c| !c.get_name().eq(&context.get_name()))
    //     .cloned()
    //     .collect::<Vec<CT>>()
    // ;
    if let Some(index) = config
        .contexts
        .iter()
        .position(|c| {
            c.get_name()
                .eq(&context.get_name())
        })
    {
        config
            .contexts
            .remove(index);
    }
    config
        .contexts
        .push(context.clone());
    // contexts.push(context.clone());
    // config.contexts = contexts;
    write_config_file(app_name, &config)?;
    Ok(())
}

// sgtools_libs - Code utilities for sgtools
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![cfg(feature = "fs")]

use std::path::Path;
use std::path::PathBuf;

use anyhow::Result;

pub fn copy_dir_all<S, D>(src: S, dst: D) -> Result<()>
where
    S: AsRef<Path>,
    D: AsRef<Path>,
{
    std::fs::create_dir_all(&dst)?;
    for entry in std::fs::read_dir(src)? {
        let entry = entry?;
        let ty = entry.file_type()?;
        if ty.is_dir() {
            copy_dir_all(
                entry.path(),
                dst.as_ref()
                    .join(entry.file_name()),
            )?;
        } else {
            std::fs::copy(
                entry.path(),
                dst.as_ref()
                    .join(entry.file_name()),
            )?;
        }
    }
    Ok(())
}

fn read_dir_loop<P>(path: P, entries: &mut Vec<PathBuf>) -> Result<()>
where
    P: AsRef<Path>,
{
    for entry in std::fs::read_dir(path)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            entries.push(path)
        } else if path.is_dir() {
            read_dir_loop(path, entries)?;
        }
    }
    Ok(())
}

pub fn read_dir<P>(path: P) -> Result<Vec<PathBuf>>
where
    P: AsRef<Path>,
{
    let mut entries = vec![];
    read_dir_loop(path, &mut entries)?;
    Ok(entries)
}

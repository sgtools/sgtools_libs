// sgtools_libs - Code utilities for sgtools
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![cfg(feature = "prompt")]

use std::path::PathBuf;

use anyhow::Result;
use inquire::error::CustomUserError;
use inquire::validator::Validation;
use inquire::Confirm;
use inquire::Select;
use inquire::Text;

use super::validate;

pub fn confirm<S: AsRef<str>>(message: S, default: bool) -> Result<bool>
{
    let response = Confirm::new(message.as_ref())
        .with_default(default)
        .prompt()?;
    Ok(response)
}

pub fn text<S: AsRef<str>>(message: S) -> Result<String>
{
    let response = Text::new(message.as_ref()).prompt()?;
    Ok(response)
}

pub fn directory<S: AsRef<str>>(message: S) -> Result<PathBuf>
{
    let response = Text::new(message.as_ref())
        .with_validator(directory_validator)
        .prompt()?;
    let mut path = PathBuf::new();
    path.push(response);
    Ok(path)
}

pub fn url<S: AsRef<str>>(message: S) -> Result<String>
{
    let response = Text::new(message.as_ref())
        .with_validator(url_validator)
        .prompt()?;
    Ok(response)
}

pub fn select<S: AsRef<str>, T: std::fmt::Display>(message: S, options: Vec<T>) -> Result<T>
{
    let response = Select::new(message.as_ref(), options).prompt()?;
    Ok(response)
}

fn directory_validator(input: &str) -> Result<Validation, CustomUserError>
{
    match validate::directory(input) {
        Ok(_) => Ok(Validation::Valid),
        Err(e) => Ok(Validation::Invalid(e.into())),
    }
}

fn url_validator(input: &str) -> Result<Validation, CustomUserError>
{
    match validate::url(input) {
        Ok(_) => Ok(Validation::Valid),
        Err(e) => Ok(Validation::Invalid(e.into())),
    }
}

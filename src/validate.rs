// sgtools_libs - Code utilities for sgtools
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![cfg(feature = "validate")]

use std::path::Path;
use std::path::PathBuf;

use anyhow::anyhow;
use anyhow::Result;
use validator::Validate;

#[derive(Debug, Validate)]
struct ValidationUrl
{
    #[validate(url)]
    value: String,
}

pub fn directory<S: AsRef<str>>(input: S) -> Result<PathBuf>
{
    let path = Path::new(input.as_ref());
    if !path.exists() {
        return Err(anyhow!("Path does not exist"));
    }
    if !path.is_dir() {
        return Err(anyhow!("Path is not a directory"));
    }
    Ok(path.to_path_buf())
}

pub fn url<S: AsRef<str>>(input: S) -> Result<()>
{
    ValidationUrl {
        value: input
            .as_ref()
            .to_string(),
    }
    .validate()
    .map_err(|e| anyhow!(e))
}

// fn validate_email(input: &String) -> Result<()>
// {
//     if !validator::validate_email(input) {
//         Err(anyhow!("Invalid email"))
//     } else {
//         Ok(())
//     }
// }

// fn validate_file(input: &String) -> Result<()>
// {
//     let path = Path::new(input);
//     if !path.exists() {
//         return Err(anyhow!("Path does not exist"));
//     }
//     if !path.is_file() {
//         return Err(anyhow!("Path is not a file"));
//     }
//     Ok(())
// }

// fn validate_directory(input: &String) -> Result<()>
// {
//     let path = Path::new(input);
//     if !path.exists() {
//         return Err(anyhow!("Path does not exist"));
//     }
//     if !path.is_dir() {
//         return Err(anyhow!("Path is not a directory"));
//     }
//     Ok(())
// }

// fn validate_number<T>(input: &String) -> Result<()>
// where
//     T: std::str::FromStr
// {
//     match input.parse::<T>() {
//         Err(_) => Err(anyhow!("Invalid {}", std::any::type_name::<T>())),
//         Ok(_) => Ok(()),
//     }
// }

// fn validate_bool(input: &String) -> Result<()>
// {
//     if !["true", "false", "1", "0"].contains(&input.to_lowercase().as_str()) {
//         return Err(anyhow!("Invalid bool"));
//     }
//     Ok(())
// }

## What's Changed in v0.3.1 
* feat: Add param::get_bool_or_default

## What's Changed in v0.3.0 
* chore: Update code formatter
* feat: Add fs read_dir

## What's Changed in v0.2.2 
* chore: Update config files

## What's Changed in v0.2.1 
* feat: History on menu
* chore: Update justfile and helix configuration

## What's Changed in v0.2.0 
* feat: Add fs feature

## What's Changed in v0.1.0 
* fix: Fix coding issue in prompt

## What's Changed in v0.0.3 
* feat: Add url validator
* chore: Add editor config files

## What's Changed in v0.0.2 
* chore: Update dependencies
* feat: Fix config to remove clap dependency
* chore: Update rustanalyzer options



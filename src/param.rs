// sgtools_libs - Code utilities for sgtools
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

//! Param functions

#![cfg(feature = "param")]

use anyhow::Result;
use clap::ArgMatches;

/// Retrieve clap argument
pub fn get_str<'a>(args: &'a ArgMatches, key: &'a str, default_value: &'a str) -> Result<&'a str>
{
    let value = match args.get_one::<String>(key) {
        None => default_value,
        Some(value) => value,
    };
    Ok(value)
}

/// Retrieve clap argument as String
pub fn get_string<'a>(args: &'a ArgMatches, key: &'a str, default_value: &'a str)
    -> Result<String>
{
    let value = get_str(args, key, default_value)?;
    let value = value.trim();
    Ok(value.to_string())
}

/// Retrieve clap argument as Boolean
pub fn get_bool<'a>(args: &'a ArgMatches, key: &'a str) -> bool
{
    args.get_flag(key)
}

/// Retrieve clap argument as Boolean
pub fn get_bool_or_default<'a>(args: &'a ArgMatches, key: &'a str, default: bool) -> bool
{
    if args.contains_id(key) {
        args.get_flag(key)
    } else {
        default
    }
}

# sgtools_libs

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/sgtools%2Fsgtools_libs?style=flat-square)](https://gitlab.com/sgtools/sgtools_libs/-/pipelines)
[![GitLab Release](https://img.shields.io/gitlab/v/release/sgtools%2Fsgtools_libs?style=flat-square)](https://gitlab.com/sgtools/sgtools_libs/-/releases)
[![Static Badge](https://img.shields.io/badge/license-GPL%203.0-blue?style=flat-square)](https://gitlab.com/sgtools/sgtools_libs/-/blob/main/LICENSE)
[![Static Badge](https://img.shields.io/badge/Open%20Source%3F-Yes!-blue?style=flat-square)](#)

> Code utilities for sgtools

**Main features**
  - prompt
  - validate
  - param
  - config
  - no_tty
  - menu

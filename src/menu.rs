// sgtools_libs - Code utilities for sgtools
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

#![cfg(feature = "menu")]

use anyhow::anyhow;
use anyhow::Result;
use colored::Colorize;
use rustyline::error::ReadlineError;
use rustyline::DefaultEditor;

pub struct Menus<F>
where
    F: Fn(String) -> Result<()>,
{
    pub current: String,
    pub menus: Vec<Menu>,
    pub has_exit_function: bool,
    pub default_action: F,
    // pub history: Vec<String>,
}

impl<F> Menus<F>
where
    F: Fn(String) -> Result<()>,
{
    pub fn new(action: F) -> Self
    {
        Self {
            current: "".to_string(),
            menus: vec![],
            has_exit_function: true,
            default_action: action,
            // history: vec![]
        }
    }

    pub fn without_exit_function(mut self) -> Self
    {
        self.has_exit_function = false;
        self
    }

    pub fn menu(mut self, menu: Menu) -> Self
    {
        self.menus
            .push(menu);
        self
    }

    pub fn message(&self) -> Option<String>
    {
        self.menus
            .iter()
            .find(|m| {
                m.name
                    .eq(&self.current)
            })
            .map(|m| m.message())
    }

    pub fn handle<S: AsRef<str>>(&self, command: S) -> Result<Result<Option<String>>>
    {
        self.menus
            .iter()
            .find(|m| {
                m.name
                    .eq(&self.current)
            })
            .map(|m| m.handle(command))
            .ok_or(anyhow!("Invalid current menu"))
    }

    pub fn run<S: AsRef<str>>(mut self, menu: S) -> Result<()>
    {
        self.current = menu
            .as_ref()
            .to_string();

        let mut rl = DefaultEditor::new()?;

        while let Some(message) = self.message() {
            // rl.set_cursor_visibility()
            let readline = rl.readline(&message);
            match readline {
                Ok(line) => {
                    rl.add_history_entry(line.clone())?;
                    // self.history.push(line.clone());
                    if self.has_exit_function && line.eq("q") {
                        break;
                    } else {
                        match self.handle(&line)?? {
                            Some(goto) if !goto.is_empty() => {
                                self.current = goto;
                                println!();
                            }
                            Some(_) => {}
                            None => (self.default_action)(line)?,
                        }
                    }
                }
                Err(ReadlineError::Interrupted) => break,
                Err(ReadlineError::Eof) => break,
                Err(err) => {
                    println!("Error: {:?}", err);
                    break;
                }
            }
        }

        Ok(())
    }
}

pub struct Menu
{
    pub name: String,
    pub show_name: bool,
    pub commands: Vec<Box<dyn MenuItemTrait>>,
}

impl Menu
{
    pub fn new<S: AsRef<str>>(name: S) -> Self
    {
        Self {
            name: name
                .as_ref()
                .to_string(),
            show_name: true,
            commands: vec![],
        }
    }
}

impl Menu
{
    pub fn with_name_hidden(mut self) -> Self
    {
        self.show_name = false;
        self
    }

    pub fn command<S1: AsRef<str>, S2: AsRef<str>, F: Fn() -> Result<()> + 'static>(
        mut self,
        command: S1,
        name: S2,
        action: F,
    ) -> Self
    {
        self.commands
            .push(Box::new(MenuItem {
                goto: None,
                command: command
                    .as_ref()
                    .to_string(),
                name: name
                    .as_ref()
                    .to_string(),
                action,
            }));
        self
    }

    pub fn goto<S1: AsRef<str>, S2: AsRef<str>>(mut self, command: S1, menu: S2) -> Self
    {
        self.commands
            .push(Box::new(MenuItem {
                goto: Some(
                    menu.as_ref()
                        .to_string(),
                ),
                command: command
                    .as_ref()
                    .to_string(),
                name: format!(">>{}", menu.as_ref()),
                action: || Ok(()),
            }));
        self
    }

    pub fn message(&self) -> String
    {
        let names = self
            .commands
            .iter()
            .map(|c| format!("[{}] {}", c.get_command(), c.get_name()))
            .collect::<Vec<String>>();
        let message = names.join(" | ");
        let name = if self.show_name {
            format!("{}: ", self.name).blue()
        } else {
            "".blue()
        };
        format!("{}{} > ", name, message.yellow())
    }

    pub fn handle<S: AsRef<str>>(&self, command: S) -> Result<Option<String>>
    {
        if let Some(command) = self
            .commands
            .iter()
            .find(|c| {
                c.get_command()
                    .eq(command.as_ref())
            })
        {
            if let Some(goto) = command.get_goto() {
                Ok(Some(goto))
            } else {
                command.handle()?;
                Ok(Some("".to_string()))
            }
        } else {
            Ok(None)
        }
    }
}

pub trait MenuItemTrait
{
    fn get_goto(&self) -> Option<String>;
    fn get_name(&self) -> String;
    fn get_command(&self) -> String;
    fn handle(&self) -> Result<()>;
}

pub struct MenuItem<F>
where
    F: Fn() -> Result<()>,
{
    pub goto: Option<String>,
    pub command: String,
    pub name: String,
    pub action: F,
}

impl<F> MenuItemTrait for MenuItem<F>
where
    F: Fn() -> Result<()>,
{
    fn handle(&self) -> Result<()>
    {
        (self.action)()?;
        Ok(())
    }

    fn get_name(&self) -> String
    {
        self.name
            .clone()
    }

    fn get_command(&self) -> String
    {
        self.command
            .clone()
    }

    fn get_goto(&self) -> Option<String>
    {
        self.goto
            .clone()
    }
}
